package com.example.projectY;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "finish", urlPatterns={"/finish"})
public class finish extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Error. Please check logs.");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        List items = (ArrayList<List>) session.getAttribute("ai");
        items.clear();

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<h1>Add Another Item or Close the Browser</h1>");
        out.println("<form action=\"index.jsp\"><input \" type=\"submit\" value=\"Add Another Item\"></form>");
        out.println("</body></html>");

    }
}
