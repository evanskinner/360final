package com.example.projectY;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

//import java.lang.module.Configuration;
import java.util.List;

public class storageDAO {
    public void allinfo (String date, String item, String type, String amount) {
        try {
            Configuration configuration = new Configuration().configure();
            SessionFactory sessionFactory = configuration.buildSessionFactory();
            Session session = sessionFactory.openSession();

            Transaction transaction = session.beginTransaction();
            String myQuery = "select max(id) from storage";
            Query query = session.createQuery(myQuery);
            List list = query.list();
            System.out.println(list.get(0));

            storage logamount = new storage();
            logamount.setId(0);
            logamount.setDate(date);
            logamount.setItem(item);
            logamount.setType(type);
            logamount.setAmount(amount);
            session.save(logamount);
            transaction.commit();
            sessionFactory.close();
            System.out.println("\n\n amounts Added \n");
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            System.out.println("ERROR");
        }
    }
}
