package com.example.projectY;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(name = "collect", urlPatterns={"/collect"})
public class collect extends HttpServlet {

    List items = new ArrayList();
    Date oldDate = new Date();
    SimpleDateFormat ymd = new SimpleDateFormat("dd/MMM/yyyy");
    String date = ymd.format(oldDate);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        HttpSession session = request.getSession();
        String item = (String) session.getAttribute("item_id");
        String type = (String) session.getAttribute("type");
        String amount = (String) session.getAttribute("amount");

        items.add(date);
        items.add(item);
        items.add(type);
        items.add(amount);

        out.println("<h1>Entry has been successfully added</h1>");
        out.println("<form action=\"tracker.html\"><input \" type=\"submit\" value=\"Enter Another Item\"></form>");
        out.println("<form action=\"results\" method=\"post\"><input \" type=\"submit\" value=\"Done\"></form>");


        out.println("</body></html>");

        session.setAttribute("ai", items);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Error. Please check logs.");
    }
}
