package com.example.projectY;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        String item = request.getParameter("item_id");
        String type = request.getParameter("type");
        String amount = request.getParameter("amount");

        if (item.isEmpty()) {
            item = "N/A";
        }
        if (type.isEmpty()) {
            type = "N/A";
        }
        if (amount.isEmpty()) {
            amount = "N/A";
        }

        boolean camount= new validation(amount).getBoolean();

        String incorrect = "<h1>Your input is not valid</h1>";
        String badamt = "<p>Amount: " + amount + "</p><p>The amount must be a number</p>";

        String correct = "<h1>Data Entered</h1>";
        String citem = "<p>Item: " + item + "</p>";

        if (!camount) {
            out.println(incorrect);
            if (!camount) {
                out.println(badamt);
            } else {
                out.println("<p> Amount: " + (Double.parseDouble(amount)) + "</p>");
            }
            out.println("<form action=\"tracker.html\"><input \" type=\"submit\" value=\"Try Again\"></form>");
        } else {
            String ctype = "<p>Type: " + type + "</p>";
            String camt = "<p>Amount: " + (Double.parseDouble(amount)) + "</p>";
            out.println(correct + citem + ctype + camt);
            out.println("<form action=\"collect\" method=\"post\"><input \" type=\"submit\" value=\"Log Entry\"></form>");
            out.println("<form action=\"tracker.html\"><input \" type=\"submit\" value=\"Re-Enter amount\"></form>");
        }
        out.println("</body></html>");

        HttpSession session = request.getSession();
        session.setAttribute("item_id", item);
        session.setAttribute("type", type);
        session.setAttribute("amount", amount);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Error. Please check logs.");
    }

}
