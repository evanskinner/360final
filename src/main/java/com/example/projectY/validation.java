package com.example.projectY;

public class validation {

    private String myDouble;

    public validation() {
        myDouble = "";
    }

    public validation(String myDouble) {
        this.myDouble = myDouble;
    }

    public String getMyDouble() { return myDouble; }

    public boolean setBoolean(String myDouble) {
        try {
            Double.parseDouble(myDouble);
        } catch(NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }

    public boolean getBoolean() { return setBoolean(getMyDouble()); }
}
