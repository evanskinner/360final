package com.example.projectY;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "results", urlPatterns={"/results"})
public class results extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        List items = (ArrayList<List>) session.getAttribute("ai");

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<table class=\"mytable\">" +
                "<thead>" +
                "<tr>" +
                "<th>DATE</th>" +
                "<th>Item</th>" +
                "<th>Type</th>" +
                "<th>Amount</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>");

        for (int i = 0; i < items.size(); i++) {
            out.println("<tr>" +
                    "<td>" + items.get(i++) + "</td>" +
                    "<td>" + items.get(i++) + "</td>" +
                    "<td>" + items.get(i++) + "</td>" +
                    "<td>" + items.get(i++) + "</td>" +
                    "</tr>");
        }
        out.println("</tbody>" + "</table>");
        out.println("<form action=\"fshservlet\" method=\"post\"><input \" type=\"submit\" value=\"Save\"></form>");
        out.println("<form action=\"finish\" method=\"get\"><input \" type=\"submit\" value=\"Delete Everything\"></form>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Error. Please check logs.");

    }
}
