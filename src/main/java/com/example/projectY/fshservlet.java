package com.example.projectY;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "fshservlet", urlPatterns={"/fshservlet"})
public class fshservlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(true);
        List items = (ArrayList<List>) session.getAttribute("ai");

        try {
            storageDAO amountsDAO = new storageDAO();
            for ( int i = 0; i < items.size(); i++) {
                amountsDAO.allinfo((String)items.get(i++),(String)items.get(i++),(String)items.get(i++),(String)items.get(i));
            }
            response.sendRedirect("finish");
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Error. Please check logs.");
    }
}
