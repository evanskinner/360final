package com.example.projectY;

public class storage {

    private int id;
    private String date;
    private String item;
    private String type;
    private String amount;

    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    public String getDate() {return date;}
    public void setDate(String date) {this.date = date;}

    public String getItem() {return item;}
    public void setItem(String item) {this.item = item;}

    public String getType() {return type;}
    public void setType(String type) {this.type = type;}

    public String getAmount() {return amount;}
    public void setAmount(String amount) {this.amount = amount;}
}